EXEC = fully-leafed-subtrees
BIN_FOLDER = target/release

.PHONY: clean test hypercube5 complete10 wheel11 bipartite_7_5 cycle10

h6_naive: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 6 -t 8 -c 3

h6_dist: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 6 -t 8 -c 3 -s dist

h5_naive: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 5 -t 8 -c 3

h5_dist: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 5 -t 8 -c 3 -s dist

random40_naive: release
	./$(BIN_FOLDER)/$(EXEC) -g random -v 40 -d 0.2 -t 8 -c 2

random40_dist: release
	./$(BIN_FOLDER)/$(EXEC) -g random -v 40 -d 0.2 -s dist -t 8 -c 2

cycle10: release
	./$(BIN_FOLDER)/$(EXEC) -g cycle

bipartite_7_5: release
	./$(BIN_FOLDER)/$(EXEC) -g bipartite -v 7 -u 5

wheel11: release
	./$(BIN_FOLDER)/$(EXEC) -g wheel -v 11

complete10: release
	./$(BIN_FOLDER)/$(EXEC) -g complete -v 10

hypercube5: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 5 -t 8 -c 3

release:
	cargo +nightly build --release 

debug:
	cargo +nightly build

test:
	cargo +nightly test

clean:
	rm -fr target/