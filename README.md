# Fully-leafed induced subtrees

This repository contains code to compute the leaf function of a graph. It is based on this
[repository](https://github.com/enadeau/fully-leafed-induced-subtrees) and this article [arxiv]((https://arxiv.org/abs/1709.09808).
However, this version use Rust instead of python and use multi-threading to achieve better performance on the main
branch-and-bound algorithm. For more information on the concurrency model used in the project, please read [algorithm.md](./algorithm.md).


These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- [Rust-lang](https://www.rust-lang.org/en-US/install.html), compiler used for the project (version 1.26.1/2018-05-25);
- [Cargo](https://crates.io/), the package manager and project build;
- [rustup](https://rustup.rs/), manage different rustc versions/channels.

`Note`: Cargo and rustup gets installed automatically if you follow the first the instruction
in the first [Rust-lang](https://www.rust-lang.org/en-US/install.html).

### Installing

The project needs to run on [Nightly Rust](https://doc.rust-lang.org/1.15.1/book/nightly-rust.html). This version allow us
to use unstable feature for benchmarking, testing and useful modules.

To install nightly use this command:

```bash
$ rustup install nightly
```

### Building and running

Here is the command to build the project without any optimization:

```bash
$ cargo +nightly build
```

To build the project with all the optimizations :

```bash
$ cargo +nightly build --release
```

Finally you can run the project in the same fashion, only changing `build` for `run`, like so:

```bash
$ cargo +nightly run
```

And in release mode:

```bash
$ cargo +nightly run --release
```

`Note`: The first time you build the project it will download/compile all the dependencies you need.
You can look at the Cargo.toml file to see dependencies of the project.

## How it works

The program calculates the `leaf function` of a graph. Here's a preview of all the options you can specify:

```bash
Usage: ./target/release/fully-leafed-subtrees [--help] [--graph [cycle|wheel|complete|bipartite|random|hypercube]] [--num-vertices number] [--num-vertices2 number]
    [--density [0.2; 0.8]] [--dimension number] [--num-contexts number] [--num-threads number] [--strategy [naive|dist]]

Options:
    -t, --num-threads number
                        Sets the number of maximum thread. The default value
                        is 0 (singlethreaded)
    -g, --graph [cycle|wheel|complete|bipartite|random|hypercube]
                        Sets the graph to use. The default value is the
                        complete graph.
    -v, --num-vertices number
                        Sets the number of vertices. The default value is 10.
    -u, --num-vertices2 number
                        Second number of vertices, only works for the
                        bipartite graph, otherwise it is ignored. The default
                        value is 10.
    -c, --num-contexts number
                        Sets the number of contexts. Must be a power of 2,
                        otherwise it gets VALUE/2. The default value is 1.
    -d, --density [0.2; 0.8]
                        Sets the density of a graph. This argument works only
                        on random graph, otherwise ignored. The default value
                        is 0.8.
    -D, --dimension number
                        Sets the dimension of an hypercube. This argument
                        works only on hypecube graph, otherwise ignored. The
                        default value is 3
    -s, --strategy [naive|dist]
                        The strategy to use to calculate the leaf potential of
                        a configuration. The default value is naive
    -h, --help          print this help menu
```

By default, the command `./target/release/fully-leafed-subtrees` will print the Usage guide previously shown.
Here an example of how to solve the leaf function for a wheel graph (11):

```bash
$ ./target/release/fully-leafed-subtrees --graph wheel --num-vertices 11

Result for wheel graph : [0, 0, 2, 2, 3, 4, 5, 2, 2, 2, 2, -2147483648, -2147483648], in 0ms
```

Another example for the complete bipartite graph of 7 and 5 vertices :

```bash
$ ./target/release/fully-leafed-subtrees --graph bipartite --num-vertices 7 --num-vertices2 5

Result for bipartite graph : [0, 0, 2, 2, 3, 4, 5, 6, 7, -2147483648, -2147483648, -2147483648, -2147483648], in 0ms
```

To solve the hypercube graph of fifth dimension with 8 threads and 4 contexts per thread:

```bash
$ ./target/release/fully-leafed-subtrees --graph hypercube --dimension 5 --num-threads 8 --num-contexts 4

Result for hypercube graph : [0, 0, 2, 2, 3, 4, 5, 4, 5, 6, 6, 6, 7, 7, 7, 8, 8, 8, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648], in 353ms
```

## Running the tests

To run the tests, simply use this command:

```bash
$ cargo +nightly test
```

If you want to see all the stdout output (e.g. The println! macros), use this command instead:

```bash
$ cargo +nightly test -- --nocapture
```

## Authors

* **Louis-Vincent Boudreault** - *Initial work* - [louis-vincent.boudre](https://gitlab.com/louis-vincent.boudre)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [Enadeau](https://github.com/enadeau/fully-leafed-induced-subtrees), for the base project made in python with the main algorithms.
* [Fully leafed induced subtrees](https://arxiv.org/abs/1709.09808), article used in this project.