# Multi-threaded branch-and-bound alogirthm

In this document we will visit differents strategies applied in this project to
optimize the leaf function and reduce its run time.

## Multi-Threading

### Reminder

Lets us remind ourselves how to basic algorithm works. Here's a picture that describe very well the lifecycle of the program:
![](images/tree.png)

As you can see we have a graph, and in each step, we choose a vertex to add or excluded. This cause the program to fork the two possibilities,
either it has been included or excluded.
The goal is the find the induced subtree with most leaves in it. So as the algorithm goes down the `decision tree`, we arrived at the end with no
available vertex. Each time we reached an end-point we look at the current subtree-size and verify if there has been another tree with the same size
whose number of leaves is greater. If not, we declare the current tree to be the subtree of size `i` with the greatest number of leaves.
After that, the program has to backtrack and try previously created alternative future (branches) to find a bigger tree with bigger leaf factor, etc.

Here's the rough pseudo-python-code for the algorithm:

```python

def leaf_function(G):
    def explore():
        u = c.next_avaible_vertex()
        if u = none:
            i = c.subtree_size
            l = c.num_leaves
            L[i] = max(L[i], l);
        else:
            c.add(u)
            explore()
            c.undo()
            c.exclude(u)
            explore()
            c.undo()
    
    c = Configuration(G)   # let c be the initial configuration of G.
    L[0] = 0;
    L = [−∞ | i <- [1..graph.size()]]
```

In the `else` condition we can clearly see the `fork` in process by calling `explore()` recursively.

### Lonely core doing all the job.

The main problem with this approach is we don't use the fact that muting a configuration can be done
independently from branch to branch.
By forking a branch we can create a copy of the current configuration and let that configuration takes its own path.
We can extend that idea to the number of threads we want. Ideally, we use a number of threads which is a power of two, since
the problem makes a sort of binary tree of decisions. 

Let say we want 8 threads to compute its own part of the decision tree, 2^3 = 8, so we have to go down 3 level in the decision tree,
collect all the configurations, the start one thread per configurations collected.

If the graph has little to no symmetries, incremeting the number of threads decrease the work linearly, since each thread are
can work 100% independently without computing doubled configuration. This however, happen very RARELY.

Here's a little picture of 8 threads distribution :
![](images/thread-partition.png)

As you can see going to the third level gave us 8 child (8 threads), and each thread will compute its own forest.

### Inter-Thread communication through MPSC

Now, with our threads we want to share informations between them. If one threads finds a new induced-subtree with a lot of leaves, we might
want to communicate this new found to other so they sync-up, and cut down their own search space if the can't find any promising configuration.

With [rust's mutli-producer-single-consumer](https://doc.rust-lang.org/std/sync/mpsc/) library we can achieve that. 
This allow us to share data without any lock and mutexes. Lock are very deadly for mutli-threading performance and cause problem with CPU cache, so
we want to avoid that at all cost.

Here's a picture of the communication ring induced by connecting every two threads with a `mpsc`:
![](images/ring.png)

When a thread finds interesting result, it passes the message around. The message contains: `(size_of_the_tree, num_leaves, sender)` where the sender is
the thread uniq number. Adding the sender prevents the message looping forever and instead do at most one loop of the communication ring.
When an other thread receives a message it reads it, compares and updates its own sets of data and passes the message around. 

If the receiver has a better solution it overwrites the message by its own data and passes the message to the next thread.

### Fibers and Context Switching

Here is a brief description of a [fiber](https://en.wikipedia.org/wiki/Fiber_(computer_science)). This project heavely inspired from fibers, by implementing
its "own" context switching mechanism (very unofficialy).

See, at this point we added threading to achieve parallelism, communication to share results and possibly reduced search space of other threads. 
However, by doing this we really just moved the problem we previously had in a single thread. A thread has to go down its entire forest many times to visit all possible branches (in the worst case). 

What if you could mark some nodes with a checkpoint and alternate the works with rounds of execution?

Using context switching allow us to share to workload by dividing the search inside a forest into different parts (workers) and alternate between those as we wish.
A context is like a "thread" however they are much lighter and they are not thread... They don't use preemptive mechanism to alternate between coworkers. We,
as the developer, decide the appropriate moment to switch between workers. 

Each worker comes with its "context" which contains all the variable it needs. This is the same idea as process scheduling in an Operating System, each process has its own context where we save and restore during and between a moment of work.

Every worker takes a different road (some process described in Multi-Threading section by going down N-level). Let say we have 2 workers inside a thread.
The first worker go do its decision tree, finds interesting results saved them and continue, etc.. 

After an K-iterations we tell him: STOP!!. It's time to your coworker to do some work, now switch context. Since the second worker never worked before,
it starts much higher in the decision tree, BUT its search space might be reduce because of the first worker who already found results. So the worker
goes down its forest and try to find better solution, avoid branches who are not promising (using its knowledge) and keep until its working time is revoked.

Each worker switch between themselves in a circular-fashion, like a round-robin algorithm. By using context-switching this allow a thread to not get (or reduce)
stuck inside a branch with no solution, thus accelerating the program.

### Conclusion

By dividing the entire decision tree with threads, we achieve parallelism. By using context-switch and inter-thread communication we achieve concurrency inside and between those threads. Here's the final algorithm in pseudo-python-code:

```python
def leaf_potential(G, num_threads, num_context):
    c = Configuration(G)   # let c be the initial configuration of G.
    L[0] = 0;
    L = [−∞ | i <- [1..graph.size()]]

    thread_confs = divide_configuration_equally(c, num_threads)
    ring = create_comm_ring_for(thread_confs.len);

    foreach t in thread_confs.len:
        spawn_thread(lambda _ :
            local_list = copy(L)
            local_conf = thread_confs[t]
            contexts = divide_configuration_equally(local_conf, num_context)
            sender = ring[t].sender
            receiver = ring[t].receiver
            while True:
                contexts.try_switch()
                current_config = contexts.current
                u = current_config.next_avaible_vertex()
                if u = none:
                    i = current_config.subtree_size
                    l = current_config.num_leaves
                    L[i] = max(L[i], l);
                    current_config.back_track()
                    sender.send((i,l,t));
                else
                    if receiver.has_new_msg?:
                        msg@(i,l,origin) = receiver.new_msg
                        local_list[i] < l:
                            local_list[i] = l
                            sender.send(msg)    # sends it to the next
                    
                    if current_config.is_promising?:
                        if current_config.is_bactracking?:
                            current_config.exclude(u)
                        else:
                            current_config.add(u)
                    else:
                        current_config.backtrack()
            sync(local_list, L);
        })
```

## Miscellaneous optimizations

Here are more technical optimization made in general to boost the performance.

### Removed recursion

Even though our graph are not huge (most of the time less than 100 nodes), we might think it's not a big deal to keep recursion.
Lets take the hypercube of 4-dimension, if we call `leaf_function` on this graph, we would have to do about 2 millions iterations.
We will never overflow the stack but the cost to call 2 millions time the same function will affect performance for sure.

By doing so, we avoid doing 2 millions stackframe allocation.

### Removed Generics (a.k.a Hash)

In the original version, we used generic Configuration to represents different graph with different labeling type like string, int, custom object.
However, by doing so we had to use hash-table to keep node-neighbors access in constant time. This is because we have to reference/copy node a lot of time (millions of time) and Hashing is no near cheap. When profiling with [Cachegrind](http://valgrind.org/docs/manual/cg-manual.html), 30% of the CPU time was spent
hashing, when we really just need numbers to represent node.

Technically we already had perfect conditions to use a fixed sized array instead of hash table:
- The sets of nodes make a perfect sequence of numbers from `0` to `n` nodes;
- The graph is fixed once its created;
- We only test/executed the program with numbers for graph labeling.

So in `configuration.rs` you can see two implementation, a generic one and a specialized version for `unsigned 32-bit integer` that is using vectors to hold
graph informations. Which is much between than hash table, more predictable, access time of `O(1)` every time. And every vector can be initialized with the proper
capacity out of the box, except for the `history` field of a configuration which grown dynamically, but not that much.

### Cache friendly

The hotpath is the main path (sequence of instruction) the program takes during its runtime. In the hotpath we want to avoid branching (if, else), by doing
so we increase the flow in the instruction pipeline of CPU (no stall, better instruction caching). 

The most called function in the entire program is `undo`.

To reduce branching (if, else) and make better use of instruction caching, the history of a configuration keeps track of more data than just its vertex. It keeps
an entire copy of the old state (old number of leaves, old number of border vertex, etc..). By doing so, it shortened the code to undo an include/exclude operation and reduce the number of if-else statement if these two functions.

The general strategy to reduce branching and be more cache friendly is: denormalizing data and using contiguous memory type data structure (like arrays or vectors).
Moreover, we don't share any data (same memory region variables) between thread, thus avoiding [false sharing](https://en.wikipedia.org/wiki/False_sharing).

### Conclusion

Here is a resume of the strategy used to gain performance:

- Array/Vector is good;
- Sharing is bad, copy is good;
- If/else if/match are evils;
- use integers everywhere;
- denormalize (goes with copying), if you have relation between two entity through a key (unique way to identify), just copy it and make a tuple;
- Cache is my only memory;
- Avoid RAM;

