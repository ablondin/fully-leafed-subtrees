use graph::*;
use std::collections::{HashMap, HashSet, BinaryHeap, BTreeSet,VecDeque};
use std::hash::{Hash};
use std::fmt::{Debug};
use std::cmp::{max, min};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Status {
    Included,
    Excluded,
    Border,
    NotSeen
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum StatusInfo<T> {
    Degree(u32),
    Vertex(T),
    Nil
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Strategy {
    Naive,
    Dist
}

use self::Status::*;
use self::StatusInfo::*;
use self::Strategy::*;

#[derive(Debug, Clone)]
pub struct Configuration<'a, T: 'a> where T : Ord + Clone + Hash + Debug {
    graph: &'a Graph<T>,
    vertex_status: HashMap<T, (Status, StatusInfo<T>)>,
    subtree_vertices: Vec<T>,
    subtree_size: usize,
    num_leaves: usize,
    num_excluded: usize,
    border_size: usize,
    pub history: Vec<T>
}

type OldState = (Status, usize, usize, usize);

#[derive(Debug,Clone)]
pub struct UInt32Configuration {
    vertex_status: Vec<(Status, StatusInfo<u32>)>,
    subtree_vertices: Vec<u32>,
    subtree_size: usize,
    num_leaves: usize,
    num_excluded: usize,
    border_size: usize,
    pub history: Vec<(u32, Status, OldState)>,
    vertex_neighbors: Vec<Vec<u32>>,
    strategy: Strategy
}

impl <'a, T> Configuration<'a, T> where T : Ord + Clone + Hash + Debug {
    
    /// Returns a configuration with a given graph
    /// 
    /// # Arguments
    /// 
    /// * `g` - A graph that holds all the nodes and vertices.
    /// 
    #[allow(dead_code)]
    pub fn new(g: &'a Graph<T>) -> Configuration<T> {
        let mut vertex_status = HashMap::with_capacity(g.nb_nodes());
        let vertices = g.nodes();
        
        for v in vertices {
            vertex_status.insert(v, (NotSeen, Nil));
        }
        
        Configuration {
            graph: g,
            vertex_status: vertex_status,
            subtree_vertices: Vec::new(),
            num_leaves: 0,
            num_excluded: 0,
            border_size: 0,
            subtree_size: 0,
            history: Vec::new()
        }
    }
    
    #[allow(dead_code)]
    pub fn to_level(&mut self, level: u32, leaf_list: &mut Vec<i32>) -> Vec<Configuration<T>> {
        let mut n = level;
        let mut backtracking = false;
        let mut out = vec![];
        loop {
            if n == 0 {
                let mut cloned_config = self.clone();
                cloned_config.history.clear();
                out.push(cloned_config);
                let (has_next, steps) = self.backtrack();
                if !has_next {
                    break;
                }
                backtracking = true;
                n += steps;
                continue;
            }
            let u = self.vertex_to_add();
            if u.is_none() {
                let i = self.subtree_size();
                let l = self.num_leaves() as i32;
                leaf_list[i] = max(leaf_list[i], l);
                let (has_next, steps) = self.backtrack();
                if !has_next {
                    break;
                }
                backtracking = true;
                n += steps;
            }
            else {
                if backtracking {
                    self.exclude_vertex(&u.unwrap());
                    backtracking = false;
                } else {
                    self.include_vertex(&u.unwrap());
                }
                n -= 1;
            }
        }
        out
    }
    
    #[allow(dead_code)]
    pub fn backtrack(&mut self) -> (bool, u32) {
        let mut step_back = 0;
        while self.history_len() > 0 {
            let (_, status) = self.undo();
            step_back += 1;
            if status == Status::Included {
                return (true, step_back);    // There's still path to discover
            }
        }
        return (false, step_back);   // No more paths to discover
    }

    /// Returns the number of operations that has been made on the configuration
    #[allow(dead_code)]
    pub fn history_len(&self) -> usize {
        self.history.len()
    }

    /// Includes a vertex to the current configuration
    /// 
    /// # Arguments
    /// 
    /// * `v`- A valid vertex whose status is either NotSeen or Border
    ///
    #[allow(dead_code)] 
    pub fn include_vertex(&mut self, v: &T) -> u32 {
        //if self.is_status(&v, Included) { return 0; }
        assert!(!self.is_status(&v, Included), 
            "v = {:?}, status = {:?}", 
            v, 
            self.vertex_status.get(&v).unwrap());

        let mut degree = 0;

        if !self.graph.has_vertex(&v) {
            return 0;
        }
        for ref neighbor in self.graph.neighbors(&v).unwrap().iter() {
            let res = self.vertex_status.get_mut(&neighbor);
            if let Some(status) = res {
                match *status {
                    (NotSeen, _) => {
                        *status = (Border, Nil);
                        self.border_size += 1;
                    },  
                    (Included, Degree(d)) => {
                        degree = d;
                        *status = (Included, Degree(d + 1));
                        if degree == 1 {
                            self.num_leaves -= 1;
                        }
                    },
                    (Border, _) => {
                        *status = (Excluded, Vertex(v.clone()));
                        self.border_size -= 1;
                        self.num_excluded += 1;
                    },
                    _ => ()
                }
            }
        }
        let mut d = 0;
        if let Some(status) = self.vertex_status.get_mut(&v) {
            if let (Border,_) = *status {
                d = 1;
                self.border_size -= 1;
            }
            *status = (Included, Degree(d));
        }

        self.num_leaves += 1;
        self.subtree_vertices.push(v.clone());
        self.subtree_size += 1;
        self.history.push(v.clone());
        degree
    }

    /// Excludes a vertex from the current configurations
    /// 
    /// # Arguments
    /// 
    /// * `v` - The vertex to remove from the configuration.
    #[allow(dead_code)] 
    pub fn exclude_vertex(&mut self, v: &T) {
        assert!(self.is_status(&v, Border) ||
                self.subtree_size == 0);

        self.vertex_status.insert(v.clone(), (Excluded, Vertex(v.clone())));

        if self.subtree_size > 0 {
            self.border_size -= 1;
        }
        self.num_excluded += 1;
        self.history.push(v.clone());
    }
    
    /// Gets the degree of a vertex, excluding `Excluded` vertex neighbors
    /// 
    /// # Arguments
    /// 
    /// * `v` - The vertex to get the degree of
    /// 
    #[allow(dead_code)]
    pub fn degree(&self, v: &T) -> usize {
        let neighbors = self.graph.neighbors(&v).unwrap();
        neighbors.iter().
            filter(|x| !self.is_status(&x, Excluded)).
            count()
    }

    /// Determines if a vertex has the given status passed in argument
    /// 
    /// # Arguments
    /// 
    /// * `v` - The vertex to test
    /// * `status` - The status to verify against `v`
    /// 
    #[allow(dead_code)]
    fn is_status(&self, v: &T, status: Status) -> bool {
        match self.vertex_status.get(&v) {
            Some((s, _)) => *s == status,
            _ => false
        }
    }

    /// Undo the last operation done on the current configuration
    #[allow(dead_code)]
    pub fn undo(&mut self) -> (T, Status) {
        assert!(self.history.len() > 0);
        let v = self.history.pop().unwrap();
        let v1 = v.clone();
        if self.is_status(&v, Included) {
            self.undo_inclusion(&v);
            (v1, Included)
        } else {
            self.undo_exclusion(&v);
            (v1, Excluded)
        }
    }

    /// Undo the last operation assuming in the was an inclusion
    /// 
    /// # Arguments
    /// 
    /// * `v` - The vertex to undo the inclusion on
    /// 
    #[allow(dead_code)]
    fn undo_inclusion(&mut self, v: &T) {
        for ref neighbor in self.graph.neighbors(&v).unwrap().iter() {
            let res = self.vertex_status.get_mut(&neighbor);
            let status  = res.unwrap();
            let mut undo_exclusion = false;
            match *status {
                (Border, _) => {
                    *status = (NotSeen, Nil);
                    self.border_size -= 1;
                },  
                (Included, Degree(ref mut d)) => {
                    //*status = (Included, Degree(d - 1));
                    if *d == 2 {
                        self.num_leaves += 1;
                    }
                    *d -= 1;
                },
                (Excluded, Vertex(ref v1)) if v1 == v => {
                    undo_exclusion = true;
                },
                _ => continue
            }
            if undo_exclusion {
                *status = (Border, Nil);
                self.border_size += 1;
                self.num_excluded -= 1;
                undo_exclusion = false;
            }
        }
        self.subtree_size -= 1;
        if self.subtree_size > 0 {
            self.vertex_status.insert(v.clone(), (Border, Nil));
            self.border_size += 1;
        } else {
            self.vertex_status.insert(v.clone(), (NotSeen, Nil));
        }
        self.subtree_vertices.pop();
        self.num_leaves -= 1;
    }

    /// Undo the last operation assuming in the was an exclusion
    /// 
    /// # Arguments
    /// 
    /// * `v` - The vertex to undo the exclusion on
    ///
    #[allow(dead_code)]
    fn undo_exclusion(&mut self, v: &T) {
        self.num_excluded -= 1;
        if self.subtree_size == 0 {
            self.vertex_status.insert(v.clone(),(NotSeen, Nil));
        } else {
            self.vertex_status.insert(v.clone(), (Border, Nil));
            self.border_size += 1;
        }
    }

    /// Determines the next available vertex that can be include/exclude
    #[allow(dead_code)]
    pub fn vertex_to_add(&self) -> Option<T> {
        if self.subtree_size == 0 {
            for (k,v) in self.vertex_status.iter() {
                if let (NotSeen, _) = v {
                    return Some(k.clone());
                }
            }
        }
        else {
            for (k,v) in self.vertex_status.iter() {
                if let (Border, _) = v {
                    return Some(k.clone());
                }
            }
        }
        None
    }

    /// Determines the leaf potential of the current configuration for a given size
    /// 
    /// # Arguments
    /// 
    /// * `i` - The size to test against
    /// 
    #[allow(dead_code)]
    pub fn leaf_potential(&self, i: usize) -> u32 {
        self.leaf_potential_weak(i)
        //self.leaf_potential_dist(i)
    }

    /// Gets the number of leaves of the current configuration
    #[allow(dead_code)]
    pub fn num_leaves(&self) -> usize {
        if self.subtree_size == 1 {
            0
        } else {
            self.num_leaves
        }
    }

    /// Gets the number of excluded vertices of the current configuration
    #[allow(dead_code)]
    pub fn num_excluded(&self) -> usize {
        self.num_excluded
    }

    /// Gets the number of included vertices of the current configuration
    #[allow(dead_code)]
    pub fn subtree_size(&self) -> usize {
        self.subtree_size
    }

    #[allow(dead_code)]
    fn leaf_potential_weak(&self, i: usize) -> u32 {
        if i > self.subtree_size + self.border_size {
            return (self.num_leaves + i - self.subtree_size - 1) as u32;
        }
        return (self.num_leaves + i - self.subtree_size) as u32;
    }

    #[allow(dead_code)]
    fn leaf_potential_dist(&self, i: usize) -> u32 {
        //assert!(self.subtree_size > 2);
        let mut n = self.subtree_size;
        let mut l = self.num_leaves;
        let y = self.border_size;

        if n + y >= i {
            l += i - n;
            n = i;
        } else {
            n += y;
            l += y;
        }
        let mut d = 1;
        let partitions = self.partition_by_distance();
        let partitions_len = partitions.len();
        
        if partitions.len() < d {
            return 0;
        }

        let mut heap = BinaryHeap::<(u32, T)>::with_capacity(partitions[0].len());
        
        let feed_heap = |h: &mut BinaryHeap<(u32, T)>, partition: &Vec<(T, u32)>| {
            for (ref v, ref d) in partition.iter() {
                if *d > 1 {
                    h.push((d.clone(),v.clone()));
                }
            }
        };
        
        feed_heap(&mut heap, &partitions[0]);

        let mut visited = HashSet::<T>::with_capacity(partitions_len);

        while n < i && !heap.is_empty() {
            let (_, v) = heap.pop().unwrap();
            if visited.contains(&v) {
                continue;
            }
            let degv = self.degree(&v);
            if n + degv - 1 <= i {
                n += degv - 1;
                l += degv - 2;
            } else {
                l += (i - n) - 1;
                n = i;
            }
            d += 1;
            visited.insert(v);
            if d < partitions_len {
                feed_heap(&mut heap, &partitions[d]);
            }
        }

        return l as u32;
    }

    #[allow(dead_code)]
    fn partition_by_distance(&self) -> Vec< Vec<(T, u32)> > {
        let mut visited = BTreeSet::<T>::new();
        let mut queue = VecDeque::<(T, u32)>::new();
        for u in self.subtree_vertices.iter() {
            if let Some((Included, Degree(ref d))) = self.vertex_status.get(&u) {
                if *d > 1 {
                    queue.push_back((u.clone(), (*d).clone()));
                }
            }
        }
        let mut vertices = Vec::< Vec<(T, u32)> >::new();
        let mut layer = Vec::<(T,u32)>::new();
        let mut prev_dist = 0;
        while !queue.is_empty() {
            let (v, dist) = queue.pop_front().unwrap();
            if !visited.contains(&v) {
                visited.insert(v.clone());
                if prev_dist < dist {
                    if prev_dist > 0 {
                        vertices.push(layer.clone());
                    }
                    layer.clear();
                }
                let mut degree = 0;
                for ref u in self.graph.neighbors(&v).unwrap().iter() {
                    match self.vertex_status.get(&u) {
                        Some((Excluded, _)) => (),
                        _ => {
                            degree += 1;
                            if !visited.contains(&u) {
                                queue.push_back(((*u).clone(), dist + 1));
                            }
                        }
                    }
                }
                layer.push((v.clone(), degree));
                prev_dist = dist;
            }
        }
        vertices.push(layer);
        vertices
    }
}



impl  UInt32Configuration {
    #[allow(dead_code)]
    pub fn new(g: &Graph<u32>, s: Strategy) -> UInt32Configuration {
        let vertex_status = vec![(NotSeen,Nil); g.nb_nodes()];
        let mut vertex_neighbors: Vec<Vec<u32>> = Vec::with_capacity(g.nb_nodes());
        for n in g.nodes().iter() {
            vertex_neighbors.push(g.neighbors(n).unwrap().clone());
        }
        UInt32Configuration {
            vertex_status: vertex_status,
            subtree_vertices: Vec::new(),
            num_leaves: 0,
            num_excluded: 0,
            border_size: 0,
            subtree_size: 0,
            history: Vec::new(),
            vertex_neighbors: vertex_neighbors,
            strategy: s
        }
    }

    /// Returns the number of operations that has been made on the configuration
    pub fn history_len(&self) -> usize {
        self.history.len()
    }

    /// Call undo as long there's history and as long as the 
    /// top of the history stack is not an Included vertex.
    /// 
    /// Returns a tuple : (history_not_empty, number_of_backtrack_steps)
    pub fn backtrack(&mut self) -> (bool, u32) {
        let mut step_back = 0;
        while self.history_len() > 0 {
            let (_, status) = self.undo();
            step_back += 1;
            if status == Status::Included {
                return (true, step_back);    // There's still path to discover
            }
        }
        return (false, step_back);   // No more paths to discover
    }

    /// Divides the current configuration into sub configurations (tree-like structure).
    /// 
    /// # Arguments
    /// 
    /// * `divisor`   - The divisor to divide the configuration. CAn be seen as the height of a binary tree.
    /// * `leaf_list` - While reaching to the level of the divisor (height), we want to keep track
    ///                 of possible optimal fully leafed induced subtrees.
    /// 
    /// Returns the a vector contains all childs at `divisor` height of the current configuration.
    /// 
    pub fn divide(&mut self, divisor: u32, leaf_list: &mut Vec<i32>) -> Vec<UInt32Configuration> {
        if divisor == 0 {
            return vec![self.clone()];
        }
        let mut n = divisor;
        let mut backtracking = false;
        let mut out = vec![];
        loop {
            if n == 0 {
                let mut cloned_config = self.clone();
                cloned_config.history.clear();
                out.push(cloned_config);
                let (has_next, steps) = self.backtrack();
                if !has_next {
                    break;
                }
                backtracking = true;
                n += steps;
                continue;
            }
            let u = self.vertex_to_add();
            if u.is_none() {
                let i = self.subtree_size();
                let l = self.num_leaves() as i32;
                leaf_list[i] = max(leaf_list[i], l);
                let (has_next, steps) = self.backtrack();
                if !has_next {
                    break;
                }
                backtracking = true;
                n += steps;
            }
            else {
                if backtracking {
                    self.exclude_vertex(u.unwrap());
                    backtracking = false;
                } else {
                    self.include_vertex(u.unwrap());
                }
                n -= 1;
            }
        }
        out
    }

    /// Includes a vertex to the current configuration
    /// 
    /// # Arguments
    /// 
    /// * `v`- A valid vertex whose status is either NotSeen or Border
    /// 
    pub fn include_vertex(&mut self, v: u32) -> u32 {
        assert!(!self.is_status(&v, Included));
        let mut degree = 0;
  
        let neighbors = &self.vertex_neighbors[v as usize];
        let (old_vertex_status, _) = self.vertex_status[v as usize];
        let old_state = (old_vertex_status, self.num_leaves, self.border_size, self.num_excluded);

        unsafe {
            for i in 0..neighbors.len() {
                let neighbor = neighbors[i];
                let status = self.vertex_status.get_unchecked_mut(neighbor as usize);
                match *status {
                    (NotSeen, _) => {
                        *status = (Border, Nil);
                        self.border_size += 1;
                    },  
                    (Border, _) => {
                        *status = (Excluded, Vertex(v.clone()));
                        self.border_size -= 1;
                        self.num_excluded += 1;
                    },
                    (Included, Degree(d)) => {
                        degree = d;
                        *status = (Included, Degree(d + 1));
                        if degree == 1 {
                            self.num_leaves -= 1;
                        }
                    },
                    _ => ()
                }
            }
        }
        let mut d = 0;
        if old_vertex_status == Border {
            d = 1;
            self.border_size -= 1;
        }
        self.vertex_status[v as usize] = (Included, Degree(d));

        self.num_leaves += 1;
        self.subtree_vertices.push(v);
        self.subtree_size += 1;
        self.history.push((v, Included, old_state));
        degree
    }

    /// Excludes a vertex from the current configuration
    /// 
    /// # Arguments
    /// 
    /// * `v` - A valid vertex whose status is Border or NotSeen if the subtree's size is 0.
    /// 
    pub fn exclude_vertex(&mut self, v: u32) {
        assert!(self.is_status(&v, Border) ||
                self.subtree_size == 0);
        let (old_status, _) = self.vertex_status[v as usize];
        self.vertex_status[v as usize] = (Excluded, Vertex(v));
        let old_state = (old_status, 0, self.border_size, 0);
        if self.subtree_size > 0 {
            self.border_size -= 1;
        }
        self.num_excluded += 1;
        self.history.push((v, Excluded, old_state));
    }

    fn is_status(&self, v: &u32, status: Status) -> bool {
        let (s, _) = self.vertex_status[*v as usize];
        s == status
    }

    /// Undo the last operation done on the current configuration
    pub fn undo(&mut self) -> (u32, Status) {
        assert!(self.history.len() > 0);
        let (v, status, old_state) = self.history.pop().unwrap();
        if status == Included {
            self.undo_inclusion(v, old_state);
        } else {
            self.undo_exclusion(v, old_state);
        }
        (v, status)
    }

   
    /// Undo the last operation assuming in the was an inclusion
    /// 
    /// # Arguments
    /// 
    /// * `v` - The vertex to undo the inclusion on
    /// 
    fn undo_inclusion(&mut self, v: u32, old_state: OldState) {
        let neighbors = &self.vertex_neighbors[v as usize];
        let (old_status, old_num_leaves, old_border_size, old_num_excluded) = old_state;
        unsafe {
            for i in 0..neighbors.len() {
                let neighbor = neighbors[i];
                let status = self.vertex_status.get_unchecked_mut(neighbor as usize);
                match *status {
                    (Border, _) => {
                        *status = (NotSeen, Nil);
                    }, 
                    (Excluded, Vertex(v1)) if v1 == v => {
                        *status = (Border, Nil);
                    },
                    (Included, Degree(ref mut d)) => {
                        *d -= 1;
                    },
                    _ => continue
                }
            }
        }
        self.num_leaves = old_num_leaves;
        self.num_excluded = old_num_excluded;
        self.border_size = old_border_size;

        self.subtree_size -= 1;
        self.vertex_status[v as usize] = (old_status, Nil);
        self.subtree_vertices.pop();
    }

    
    /// Undo the last operation assuming in the was an exclusion
    /// 
    /// # Arguments
    /// 
    /// * `v` - The vertex to undo the exclusion on
    ///
    fn undo_exclusion(&mut self, v: u32, old_state: OldState) {
        let (old_status, _, old_border_size, _) = old_state;
        self.num_excluded -= 1;
        self.vertex_status[v as usize] = (old_status, Nil);
        self.border_size = old_border_size;
    }

    /// Determines the next available vertex that can be include/exclude
    pub fn vertex_to_add(&self) -> Option<u32> {
        if self.subtree_size == 0 {
            for i in 0..self.vertex_status.len() {
                if let (NotSeen, _) = self.vertex_status[i] {
                    return Some(i as u32);
                }
            }
        }
        else {
            for i in 0..self.vertex_status.len() {
                if let (Border, _) = self.vertex_status[i] {
                    return Some(i as u32);
                }
            }
        }
        None
    }

    /// Determines the leaf potential of the current configuration for a given size
    /// 
    /// # Arguments
    /// 
    /// * `i` - The size to test against
    /// 
    pub fn leaf_potential(&self, i: usize) -> u32 {
        if self.strategy == Naive {
            if i > self.subtree_size + self.border_size {
                return (self.num_leaves + i - self.subtree_size - 1) as u32;
            }
            return (self.num_leaves + i - self.subtree_size) as u32;
        }
        else {
            return self.leaf_potential_dist(i);
        }
        
    }

    #[allow(dead_code)]
    fn partition_by_distance(&self) -> Vec< Vec<(u32, u32)> >{
        assert!(self.subtree_size > 2);
        
        let mut visited = BTreeSet::<u32>::new();
        let mut queue = VecDeque::<(u32, u32)>::new();
        
        for u in self.subtree_vertices.iter() {
            if let (Included, Degree(d)) = self.vertex_status[*u as usize] {
                if d > 1 {
                    queue.push_back((*u, d));
                }
            }
        }

        let mut layer = vec![];
        let mut vertices = vec![];
        let mut prev_dist = 0;

        while !queue.is_empty() {
            let (v, dist) = queue.pop_front().unwrap();
            if !visited.contains(&v) {
                visited.insert(v);
                if prev_dist < dist {
                    if prev_dist > 0 {
                        vertices.push(layer.clone());
                    }
                    layer.clear();
                }
                let mut degree = 0;
                for u in self.vertex_neighbors[v as usize].iter() {
                    let (status, _) = self.vertex_status[*u as usize];
                    if status != Excluded {
                        degree += 1;
                        if !visited.contains(u) {
                            queue.push_back((*u, dist + 1));
                        }
                    }
                }
                layer.push((v, degree));
                prev_dist = dist;
            }
        }
        vertices.push(layer);
        vertices
    }

    #[allow(dead_code)]
    fn leaf_potential_dist(&self, i: usize) -> u32 {
        let mut current_size = self.subtree_size;
        let mut current_leaf = self.num_leaves;
        let mut lp_dist_dict = HashMap::<usize, usize>::new();
        lp_dist_dict.insert(current_size, current_leaf);

        let vertices_by_dist = self.partition_by_distance();
        for (v,_) in &vertices_by_dist[0] {
            if self.vertex_status[*v as usize].0 == Border {
                current_size += 1;
                current_leaf += 1;
                lp_dist_dict.insert(current_size, current_leaf);
            }
        }

        let max_size = current_size + vertices_by_dist[1..].iter().fold(0, |acc,layer| acc + layer.len());
        let mut current_dist = 1;
        let priority_queue: Vec<(u32, u32)> = vertices_by_dist[0].iter().map(|(u,d)| (*d,*u)).collect();
        let mut priority_queue = BinaryHeap::from(priority_queue);

        while current_size < max_size && !priority_queue.is_empty() {
            let (d,_) = priority_queue.pop().unwrap();

            if current_dist < vertices_by_dist.len() {
                for (v,d) in &vertices_by_dist[current_dist] {
                    if *d > 1 {
                        priority_queue.push((*d, *v));
                    }
                }
                current_dist += 1;
            }
            current_dist -= 1;
            let leaf_to_add = min(d - 1, (max_size-current_size) as u32);
            for _ in 0..leaf_to_add {
                current_size += 1;
                current_leaf += 1;
                lp_dist_dict.insert(current_size, current_leaf);
            }
        }
        if lp_dist_dict.contains_key(&i) {
            return lp_dist_dict[&i] as u32;
        }
        return 0;
    }

    /// Gets the number of leaves of the current configuration
    pub fn num_leaves(&self) -> usize {
        if self.subtree_size == 1 {
            0
        } else {
            self.num_leaves
        }
    }

    /// Gets the number of excluded vertices of the current configuration
    pub fn num_excluded(&self) -> usize {
        self.num_excluded
    }

    /// Gets the number of included vertices of the current configuration
    pub fn subtree_size(&self) -> usize {
        self.subtree_size
    }

}

#[cfg(test)]
mod tests {

    use configuration::*;

    #[cfg(test)]
    fn simplify_status(c: &Configuration<u32>) -> Vec<(u32, Status)> {
        let mut vec = Vec::<(u32,Status)>::new();
        for (v, s) in c.vertex_status.iter() {
            let (status,_) = s;
            vec.push((*v, *status));
        }
        vec.sort_by(|(x,_), (y,_)| x.cmp(y));
        vec
    }

    #[cfg(test)]
    fn simplify_status_u32_conf(c: &UInt32Configuration) -> Vec<(u32, Status)> {
        let mut vec = Vec::<(u32,Status)>::new();
        let mut v = 0;
        for (status, statusInfo) in c.vertex_status.iter() {
            vec.push((v, *status));
            v += 1;
        }
        vec.sort_by(|(x,_), (y,_)| x.cmp(y));
        vec
    }

    #[test]
    fn test1() {
        let g = &Graph::<u32>::complete_graph(4);
        let mut c = Configuration::new(&g);
        c.include_vertex(&0);
        c.include_vertex(&1);
        c.include_vertex(&2);
        assert_eq!(None, c.vertex_to_add());
    }


    #[test]
    fn test3() {
        let g = &Graph::<u32>::complete_graph(4);
        let mut c = Configuration::new(&g);
        c.include_vertex(&0);
        c.include_vertex(&1);
        c.include_vertex(&2);
        let expected = vec![(0, Included), (1, Included), (2, Included), (3, Excluded)];
        assert_eq!(expected, simplify_status(&c));
    }


    #[test]
    fn test4() {
        let g = &Graph::<u32>::complete_graph(4);
        let mut c = Configuration::new(&g);
        c.include_vertex(&0);
        c.include_vertex(&1);
        let expected = vec![(0, Included), (1, Included), (2, Excluded), (3, Excluded)];
        assert_eq!(expected, simplify_status(&c));
    }


    #[test]
    fn test5() {
        let g = &Graph::<u32>::complete_graph(4);
        let mut c = Configuration::new(&g);
        c.exclude_vertex(&0);
        let expected = vec![(0, Excluded), (1, NotSeen), (2, NotSeen), (3, NotSeen)];
        assert_eq!(expected, simplify_status(&c));
    }


    #[test]
    fn test6() {
        let g = &Graph::<u32>::complete_graph(4);
        let mut c = Configuration::new(&g);
        c.exclude_vertex(&0);
        c.exclude_vertex(&1);
        c.exclude_vertex(&2);
        assert_eq!(Some(3), c.vertex_to_add());
    }

    #[test]
    fn test7() {
        let g = &Graph::<u32>::complete_graph(4);
        let mut c = Configuration::new(&g);
        c.include_vertex(&0);
        c.include_vertex(&1);
        c.undo();
        let expected = vec![(0, Included), (1, Border), (2, Border), (3, Border)];
        assert_eq!(expected, simplify_status(&c));
    }


    #[test]
    fn test1_uint32_configuration() {
        let g = &Graph::<u32>::complete_graph(4);
        let mut c = UInt32Configuration::new(&g, Naive);
        c.include_vertex(0);
        c.include_vertex(1);
        c.include_vertex(2);
        assert_eq!(None, c.vertex_to_add());
    }


    #[test]
    fn test3_uint32_configuration() {
        let g = &Graph::<u32>::complete_graph(4);
        let mut c = UInt32Configuration::new(&g, Naive);
        c.include_vertex(0);
        c.include_vertex(1);
        c.include_vertex(2);
        let expected = vec![(0, Included), (1, Included), (2, Included), (3, Excluded)];
        assert_eq!(expected, simplify_status_u32_conf(&c));
    }


    #[test]
    fn test4_uint32_configuration() {
        let g = &Graph::<u32>::complete_graph(4);
        let mut c = UInt32Configuration::new(&g, Naive);
        c.include_vertex(0);
        c.include_vertex(1);
        let expected = vec![(0, Included), (1, Included), (2, Excluded), (3, Excluded)];
        assert_eq!(expected, simplify_status_u32_conf(&c));
    }


    #[test]
    fn test5_uint32_configuration() {
        let g = &Graph::<u32>::complete_graph(4);
        let mut c = UInt32Configuration::new(&g, Naive);
        c.exclude_vertex(0);
        let expected = vec![(0, Excluded), (1, NotSeen), (2, NotSeen), (3, NotSeen)];
        assert_eq!(expected, simplify_status_u32_conf(&c));
    }


    #[test]
    fn test6_uint32_configuration() {
        let g = &Graph::<u32>::complete_graph(4);
        let mut c = UInt32Configuration::new(&g, Naive);
        c.exclude_vertex(0);
        c.exclude_vertex(1);
        c.exclude_vertex(2);
        assert_eq!(Some(3), c.vertex_to_add());
    }

    #[test]
    fn test7_uint32_configuration() {
        let g = &Graph::<u32>::complete_graph(4);
        let mut c = UInt32Configuration::new(&g, Naive);
        c.include_vertex(0);
        c.include_vertex(1);
        c.undo();
        let expected = vec![(0, Included), (1, Border), (2, Border), (3, Border)];
        assert_eq!(expected, simplify_status_u32_conf(&c));
    }
}
