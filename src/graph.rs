use std::collections::{BTreeMap, BTreeSet};
use std::collections::btree_map::Iter;

extern crate rand;
use self::rand::{thread_rng, Rng};

#[derive(Debug, Clone)]
pub struct Graph<T> where T : Ord + Clone {
    nodes: BTreeMap<T,Vec<T>>
}

impl <T> Graph<T> where T: Ord + Clone {

    pub fn new() -> Graph<T> {
        let nodes = BTreeMap::<T, Vec<T>>::new();
        Graph { nodes } 
    } 

    #[allow(dead_code)]
    pub fn basic_graph(n: u32) -> Graph<u32> {
        //let mut nodes = Vec::with_capacity(n);
        //let edges = Vec::new();
        let mut nodes = BTreeMap::new();
        for i in 0..n {
            nodes.insert(i, Vec::new());
        }
        Graph { nodes }
    }

    #[allow(dead_code)]
    pub fn complete_graph(n: u32) -> Graph<u32> {
        //let mut g:L = Graph::basic_graph(n);
        let mut g = Graph::<u32>::basic_graph(n);
        for i in 0..n-1 {
            for j in i+1..n {
                g.add_edge(&i,&j);
            }
        }
        g
    }

    #[allow(dead_code)]
    pub fn cycle_graph(n: u32) -> Graph<u32> {
        let mut g = Graph::<u32>::basic_graph(n);
        for i in 0..n {
            let mut j = i + 1;
            if i == n - 1 {
                j = 0;
            }
            g.add_edge(&i,&j);
        } 
        g
    }

    #[allow(dead_code)]
    pub fn complete_bipartite_graph(m: u32, n: u32) -> Graph<u32> {
        let mut g = Graph::<u32>::basic_graph(m+n);
        for i in 0..m {
            for j in m..(m+n) {
                g.add_edge(&i, &j);
            }
        } 
        g
    }
    
    #[allow(dead_code)]
    pub fn wheel_graph(n: u32) -> Graph<u32> {
        let mut g = Graph::<u32>::cycle_graph(n);
        let k = n;
        g.nodes.insert(k, Vec::new());
        for i in 0..n {
            g.add_edge(&k, &i);
        }
        g
    }
    
    #[allow(dead_code)]
    pub fn star_graph(n: u32) -> Graph<u32> {
        Graph::<u32>::complete_bipartite_graph(1, n)
    }
    
    fn merge(&mut self, mut other: Graph<T>) {
        self.nodes.append(&mut other.nodes); 
    } 
    
    pub fn hyper_cube(dim: u32) -> Graph<u32> {
        Graph::<u32>::hyper_cube2(dim, 0) 
    }

    #[allow(dead_code)]
    fn hyper_cube2(dim: u32, at: u32) -> Graph<u32> {
        assert!(dim > 1);
        let k = 2u32.pow(dim);
        if dim == 2 {
            let mut g = Graph::<u32>::new();
            for i in at..at+k {
                g.nodes.insert(i, Vec::new());
            }
            for i in at..at+k {
                let mut j = i + 1;
                if j % 4 == 0 {
                    j = i - (i % 4);
                }
                g.add_edge(&i, &j);
            }

            return g;
        }
        let n = k/2;
        let mut g1 = Graph::<u32>::hyper_cube2(dim-1, at);
        let g2 = Graph::<u32>::hyper_cube2(dim-1, at+n);
        g1.merge(g2);

        for i in at..at+n {
            let j = i + n;
            g1.add_edge(&i,&j);
        }

        g1
    }

    /// Calcultes the number of edges from a density [0.2;1]
    /// The minimum density must be 0.2 since we only want connected graph. Otherwise
    /// They would not be conncted.
    fn edges_from_density(n: u32, density: f32) -> u32 {
        assert!(n > 0 && density >= 0.2);
        (density/ 2.0 * (n * (n-1)) as f32).floor() as u32
    }

    #[allow(dead_code)]
    pub fn random_graph(n: u32, density: f32) -> Graph<u32> {
        let mut g = Graph::<u32>::new();
        let mut remaining_edge = Graph::<u32>::edges_from_density(n, density) as i32;
        let mut rng = thread_rng();
        for i in 0..n {
            g.nodes.insert(i, Vec::new());
            if i == 0 {
                continue;
            }
            let j: u32 = rng.gen_range(0, i);
            g.add_edge(&i,&j);
            remaining_edge -= 1;
        }
        while remaining_edge > 0 {
            let i = rng.gen_range(0,n);
            let j = rng.gen_range(0,n);
            if i == j || g.has_edge(&i, &j) {
                continue;
            }
            g.add_edge(&i, &j);
            remaining_edge -= 1;
        }
        g
    }


    #[allow(dead_code)]
    pub fn custom_graph1() -> Graph<u32> {
        let mut g = Graph::<u32>::basic_graph(8); // graph inside the article, in figure 2.
        g.add_edge(&0, &2);
        g.add_edge(&0, &4);
        g.add_edge(&0, &5);
        g.add_edge(&1, &2);
        g.add_edge(&1, &6);
        g.add_edge(&2, &3);
        g.add_edge(&2, &6);
        g.add_edge(&2, &7);
        g
    }

    #[allow(dead_code)]
    pub fn nb_nodes(&self) -> usize {
        self.nodes.len()
    }

    #[allow(dead_code)]
    pub fn nb_edges(&self) -> usize {
        self.nodes.iter().fold(0, |acc, (_,v)| acc + v.len()) / 2 
    }

    /// Depth first search
    fn dfs(&self, node: &T, visited: &mut BTreeSet<T>) {
        if visited.contains(&node) {
            return;
        }
        visited.insert(node.clone());
        if let Some(neighbors) = self.neighbors(&node) {
            for neighbor in neighbors {
                self.dfs(neighbor, visited);
            }
        }
    }

    #[allow(dead_code)]
    pub fn is_connected(&self) -> bool {
        let mut visited = BTreeSet::new();
        let node = self.nodes.keys().next();
        if node.is_some() {
            self.dfs(&(node.unwrap()), &mut visited);
            return visited.len() == self.nodes.len();
        }
        return false;    // null graphs
    }

    fn add_edge(&mut self, v1: &T, v2: &T) {
        let mut add_edge2 = |x: &T, y: &T| -> () {
            let value = self.nodes.get_mut(&x);
            match value {
                Some(neigbors) => neigbors.push(y.clone()),
                _ => ()
            }
        };
        add_edge2(&v1,&v2);
        add_edge2(&v2,&v1);
    }

    #[allow(dead_code)]
    pub fn iter(&self) -> Iter<T,Vec<T>> {
        self.nodes.iter()
    }

    #[allow(dead_code)]
    pub fn nodes(&self) -> Vec<T> {
        self.nodes.keys().cloned().collect()
    }

    #[allow(dead_code)]
    pub fn neighbors(&self, v: &T) -> Option< &Vec<T> > {
        self.nodes.get(&v)
    }
    
    #[allow(dead_code)]
    pub fn has_vertex(&self, v1: &T) -> bool {
        self.nodes.contains_key(&v1)
    }
    
    #[allow(dead_code)]
    pub fn has_edge(&self, v1: &T, v2: &T) -> bool {
        match self.neighbors(&v1) {
            Some(neighbors) => {
                match neighbors.iter().position(|v: &T| v == v2) {
                    Some(_) => true,
                    _ => false
                }
            }
            _ => false
        }
    }
}

#[cfg(test)]
fn to_pretty_graph<'a>(g: &'a Graph<u32>) -> Vec< (u32, Vec<u32>) > {
    let ns = g.nodes();
    let vec: Vec<(u32, Vec<u32>)> = ns.iter().
        map(|&n| (n, g.neighbors(&n).unwrap().clone()) ).
        collect();
    vec
}

#[test]
fn test_complete_graph_10() {
    let n = 10;
    let g = Graph::<u32>::complete_graph(10);
    let expected = 45;
    assert_eq!(expected, g.nb_edges() as u32);
}

#[test]
fn test_hypercube4_edges_len() {
    let n = 4;
    let g = Graph::<u32>::hyper_cube(n);
    let expected = 2u32.pow(n-1) * n; 
    assert_eq!(expected, g.nb_edges() as u32);
}

#[test]
fn test_hypercube4_nodes_len() {
    let n = 4;
    let g = Graph::<u32>::hyper_cube(n);
    let expected = 2u32.pow(n);
    assert_eq!(expected, g.nb_nodes() as u32);
}

#[test]
fn test_hypercube4_neighbors_len() {
    let n = 6;
    let g = Graph::<u32>::hyper_cube(n);
    let expected = true;
    let actual = g.iter().all(|(k,v)| v.len() as u32 == n); 
    assert_eq!(expected, actual);
}

#[test]
fn test_random_graph_connectness_100_0dot8() {
    let g = Graph::<u32>::random_graph(100, 0.8);
    assert!(g.is_connected());
}

#[test]
fn test_random_graph_connectness_500_0dot6() {
    let g = Graph::<u32>::random_graph(500, 0.6);
    assert!(g.is_connected());
}

#[test]
fn test_random_graph_edges_100_0dot7() {
    let edges = Graph::<u32>::edges_from_density(100, 0.7);
    let g = Graph::<u32>::random_graph(100, 0.7);
    assert_eq!(edges, g.nb_edges() as u32);
}

#[test]
fn test_random_graph_edges_100_0dot2() {
    let edges = Graph::<u32>::edges_from_density(100, 0.2);
    let g = Graph::<u32>::random_graph(100, 0.2);
    assert_eq!(edges, g.nb_edges() as u32);
}

#[test]
fn test_edges_from_density_of_0dot8() {
    let expected = 36;
    let actual = Graph::<u32>::edges_from_density(10,0.8);
    assert_eq!(expected, actual);
}

#[test]
fn test_edges_from_density_of_1() {
    let expected = 45;
    let actual = Graph::<u32>::edges_from_density(10, 1.0);
    assert_eq!(expected, actual);
}


#[test]
fn test_edges_from_density_of_0dot2() {
    let expected = 9;
    let actual = Graph::<u32>::edges_from_density(10,0.2);
    assert_eq!(expected, actual);
}

#[test]
fn test_edges_from_density_of_0dot3() {
    let expected = 228228;
    let actual = Graph::<u32>::edges_from_density(1234,0.3);
    assert_eq!(expected, actual);
}