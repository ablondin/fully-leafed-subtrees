#![feature(duration_extras, test)]

mod graph;
mod configuration;
mod solver;
use std::time::{Instant};

extern crate getopts;
use std::env;
use getopts::Options;

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [--help] [--graph [cycle|wheel|complete|bipartite|random|hypercube]] [--num-vertices number] [--num-vertices2 number]
    [--density [0.2; 0.8]] [--dimension number] [--num-contexts number] [--num-threads number] [--strategy [naive|dist]]", program);
    print!("{}", opts.usage(&brief));
}


fn main() {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optopt("t", "num-threads",     "Sets the number of maximum thread.
                                         The default value is 0 (singlethreaded)", 
                                        "number");
                                        
    opts.optopt("g", "graph",           "Sets the graph to use.
                                         The default value is the complete graph.", 
                                        "[cycle|wheel|complete|bipartite|random|hypercube]");
                                        
    opts.optopt("v", "num-vertices",    "Sets the number of vertices.
                                         The default value is 10.", 
                                        "number");

    opts.optopt("u", "num-vertices2",   "Second number of vertices, only works for the bipartite graph, otherwise it is ignored.
                                         The default value is 10.", 
                                        "number");

    opts.optopt("c", "num-contexts",    "Sets the number of contexts. Must be a power of 2, otherwise it gets VALUE/2.
                                         The default value is 1.", 
                                        "number");

    opts.optopt("d", "density",         "Sets the density of a graph. This argument works only on random graph, otherwise ignored.
                                         The default value is 0.8.", 
                                        "[0.2; 0.8]");
                                        
    opts.optopt("D", "dimension",       "Sets the dimension of an hypercube. This argument works only on hypecube graph, otherwise ignored.
                                         The default value is 3", 
                                        "number");
                                        
    opts.optopt("s", "strategy",        "The strategy to use to calculate the leaf potential of a configuration.
                                         The default value is naive",
                                        "[naive|dist]");

    opts.optflag("h", "help", "print this help menu");

    if args.len() == 1 {
        print_usage(&program, opts);
        return;
    }

    
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(f) => { panic!(f.to_string()) }
    };

    if matches.opt_present("h") {
        print_usage(&program, opts);
        return;
    }

    if matches.opt_present("g") {
        if matches.opt_str("g").is_none() {
            print_usage(&program, opts);
            return;
        }

        let mut num_vertices: u32 = 10;
        let mut num_vertices2: u32 = 10;

        let mut density: f32 = 0.8;
        let mut num_threads: u32 = 0;
        let mut num_contexts: u32 = 0;

        let mut strategy = configuration::Strategy::Naive;
        
        // strategy
        if matches.opt_present("s") && matches.opt_str("s").is_some() {
            match matches.opt_str("s").unwrap().as_str() {
                "dist" => strategy = configuration::Strategy::Dist,
                _ => ()
            }
        }

        // num-vertices
        if matches.opt_present("v") && matches.opt_str("v").is_some() {
            num_vertices = matches.opt_str("v").unwrap().parse().unwrap();
        }

        // density
        if matches.opt_present("d") && matches.opt_str("d").is_some() {
            density = matches.opt_str("d").unwrap().parse().unwrap();
        }

        // num-threads
        if matches.opt_present("t") && matches.opt_str("t").is_some() {
            num_threads = matches.opt_str("t").unwrap().parse().unwrap();
        }

        // num-contexts
        if matches.opt_present("c") && matches.opt_str("c").is_some() {
            num_contexts = matches.opt_str("c").unwrap().parse().unwrap();
        }

        let graph_type = matches.opt_str("g").unwrap();
        let mut graph: Option<graph::Graph<u32>> = None;
        match graph_type.as_str() {
            "cycle"  => graph = Some(graph::Graph::<u32>::cycle_graph(num_vertices)),
            "complete"  =>  graph = Some(graph::Graph::<u32>::complete_graph(num_vertices)),
            "bipartite" =>  {
                if matches.opt_present("u") && matches.opt_str("u").is_some() {
                    num_vertices2 = matches.opt_str("u").unwrap().parse().unwrap();
                }
                graph = Some(graph::Graph::<u32>::complete_bipartite_graph(num_vertices, num_vertices2));
            },
            "wheel"  =>  graph = Some(graph::Graph::<u32>::wheel_graph(num_vertices)),
            "random" =>  graph = Some(graph::Graph::<u32>::random_graph(num_vertices, density)),
            "hypercube" => { 
                let mut dim: u32 = 3;
                if matches.opt_present("D") && matches.opt_str("D").is_some() {
                    dim = matches.opt_str("D").unwrap().parse().unwrap();
                }
                graph = Some(graph::Graph::<u32>::hyper_cube(dim));
            }
            unknown => {
                println!("Unknown type: {} for a graph", unknown);
            }
        }

        if graph.is_none() {
            print_usage(&program, opts);
            return;
        }

        let graph = graph.unwrap();
        let mut result = vec![];
        let now = Instant::now();
        if num_threads == 0 {
            result = solver::Solver::leaf_function(&graph, num_contexts/2, strategy);
        } else {
            result = solver::Solver::leaf_function_threaded(&graph, num_threads, num_contexts/2, strategy);
        }
        let duration = now.elapsed();
        let ms = (duration.as_secs() * 1_000) + (duration.subsec_nanos() / 1_000_000) as u64;
        println!("Result for {} graph : {:?}, in {}ms", graph_type, result, ms);
    }
}
