extern crate test;
extern crate rand;

use configuration::*;
use graph::*;
use std::cmp::{max};
use std::thread;
use std::sync::mpsc;
use std::sync::{Arc, Mutex};

pub struct Solver;

impl Solver {
    #[allow(dead_code)]
    pub fn leaf_function(g: &Graph<u32>, 
                         nb_context: u32,
                         strategy: Strategy) -> Vec<i32> {
        let mut c = UInt32Configuration::new(&g, strategy);
        let nb_nodes = g.nb_nodes();
        let mut list = vec![i32::min_value();nb_nodes+1];
        list[0] = 0;
        let mut backtracking = false;
        let mut contexts = c.divide(nb_context, &mut list);
        let mut dead_contexts = vec![false;contexts.len()];
        let ctx_backtracking = vec![false; contexts.len()]; // keep backtracing variable for each context
        let mut nb_dead_contexts = 0;

        const ITERATION_SLICE: u32 = 1000;
        let mut until_switch = 0;
        let mut k = 0;  // k = indice for current index.
        let max_k = contexts.len();
        
        let mut ctx_ptr: *mut UInt32Configuration = &mut contexts[k];
        let mut current_config: &mut UInt32Configuration = unsafe { &mut *ctx_ptr };
        
        'outer: while nb_dead_contexts < contexts.len() {
            if until_switch == 0 {
                until_switch = ITERATION_SLICE;
                loop {
                    k += 1;
                    if k == max_k {
                        k = 0;
                    }
                    if !dead_contexts[k] { break }
                }
                ctx_ptr = &mut contexts[k];
                current_config = unsafe { &mut *ctx_ptr };
                backtracking = ctx_backtracking[k];
            }
            until_switch -= 1;

            let u0 = current_config.vertex_to_add();
            if u0.is_none() {
                let i = current_config.subtree_size();
                let l = current_config.num_leaves();
                list[i] = max(list[i], l as i32);
                if let (true, _) = current_config.backtrack() {
                    backtracking = true;
                } else {
                    dead_contexts[k] = true;
                    nb_dead_contexts += 1;
                    until_switch = 0;
                }
                backtracking = true;      
            } else {
                let u = u0.unwrap();
                let mut promising = false;
                let n = current_config.subtree_size();
                let m = nb_nodes + 1 - current_config.num_excluded();

                for i in n..m {
                    if n <= 2 {
                        promising = true;
                        break; 
                    }
                    if list[i] < current_config.leaf_potential(i) as i32 {
                        promising = true;
                        break;
                    }
                }
                
                if !promising {
                    if let (true,_) = current_config.backtrack() {
                        backtracking = true;
                    }
                    else {  
                        until_switch = 0;
                        dead_contexts[k] = true;
                        nb_dead_contexts += 1;
                    }
                    continue;
                }
                
                if !backtracking {
                    current_config.include_vertex(u);
                } else {
                    backtracking = false;
                    current_config.exclude_vertex(u);
                }
            }
        }
        list
    } 

    #[allow(unused_must_use)]
    pub fn leaf_function_threaded(g: &Graph<u32>, max_threads: u32, 
                                  context_per_thread: u32,
                                  strategy: Strategy) -> Vec<i32> {
        // max_threads must be a power of two.
        let nb_nodes = g.nb_nodes();
        let mut list = vec![i32::min_value();nb_nodes+1];
        list[0] = 0;
        
        let mut n = 1;
        match max_threads {
            32 => n = 5,
            16 => n = 4,
            8 => n = 3,
            4 => n = 2,
            _ => ()
        }
        
        let mut configuration = UInt32Configuration::new(&g, strategy);

        
        let threads_configuration = configuration.divide(n as u32, &mut list); 
        // Here divide might not divide the configuration equally. So the final number of thread might
        // be less than expected.
        let max_threads = threads_configuration.len();
        
        // Very ugly type definition, because of the receiver that cannot be copied.
        let mut ring: Vec<(mpsc::Sender<(usize, i32, usize)>, Arc< Mutex< mpsc::Receiver<(usize, i32, usize)> > >)> = vec![];
        for _ in 0..max_threads {
            let (s,r) = mpsc::channel();
            ring.push((s, Arc::new(Mutex::new(r))));
        }

        let mut handles = vec![];
        let final_list = Arc::new(Mutex::new(list.clone()));

        for t in 0..max_threads {
            let builder = thread::Builder::new().name(t.to_string());

            // Preclone every object that you need, so when you move
            // inside the thread, you will move only shadowed version of the 
            // original variable.
            let mut local_list = list.clone();
            let mut shared_list = Arc::clone(&final_list);
            let mut local_configuration = threads_configuration[t as usize].clone();
            
            let sender = ring[t as usize].0.clone();
            let mut t2 = (t + 1) % max_threads;
            // unfortunately, you can not move our clone the receive end of a mpsc.
            // Even if you know what you are doing with your code...
            // However, you can hack it by wrapping it inside a mutex then inside an Arc, to clone it.
            let receiver = Arc::clone(&ring[t2 as usize].1);
            
            let handle = builder.spawn(move || {
                let mut backtracking = false;

                // There's only one owner for each receiver at all time, so its safe to
                // lock it at the beginning.
                let receiver = receiver.lock().unwrap();    
                // Divide into contexts.
                let mut contexts = local_configuration.divide(context_per_thread, &mut local_list);
                let mut dead_contexts = vec![false; contexts.len()];    // keep track of which context has finished
                let ctx_backtracking = vec![false; contexts.len()]; // keep backtracing variable for each context
                let mut nb_dead_contexts = 0;

                const ITERATION_SLICE: u32 = 20000; // `Sharing` factor
                let mut until_switch = 0;
                let mut k = 0;  // k = indice for current index.
                let max_k = contexts.len();
                
                let mut ctx_ptr: *mut UInt32Configuration = &mut contexts[k];
                let mut current_config: &mut UInt32Configuration = unsafe { &mut *ctx_ptr };

                'outer: while nb_dead_contexts < contexts.len() {

                    // Here we verify if it's the time to switch
                    if until_switch == 0 {
                        until_switch = ITERATION_SLICE;
                        loop {
                            k += 1;
                            if k == max_k {
                                k = 0;
                            }
                            if !dead_contexts[k] { break }
                        }
                        ctx_ptr = &mut contexts[k];
                        current_config = unsafe { &mut *ctx_ptr };
                        backtracking = ctx_backtracking[k];
                    }
                    until_switch -= 1;

                    let u0 = current_config.vertex_to_add();
                    if u0.is_none() {
                        let i = current_config.subtree_size();
                        let l = current_config.num_leaves() as i32;
                        if l > local_list[i] {
                            local_list[i] = l as i32;
                            sender.send((i, l, t));
                        }
                        if let (true, _) = current_config.backtrack() {
                            backtracking = true;
                        } else {
                            dead_contexts[k] = true;
                            nb_dead_contexts += 1;
                            until_switch = 0;
                        }
                        backtracking = true;      
                    } else {
                        let u = u0.unwrap();
                        
                        let mut promising = false;
                        let n = current_config.subtree_size();
                        let m = nb_nodes + 1 - current_config.num_excluded();
                        
                        if let Ok((i, l, origin)) = receiver.try_recv() {
                            if origin != t && local_list[i] <= l {
                                local_list[i] = l;
                                sender.send((i,local_list[i], origin)); // pass the message around
                            }
                        }
                        for i in n..m {
                            if n <= 2 { 
                                promising = true;
                                break; 
                            }
                            if local_list[i] < current_config.leaf_potential(i) as i32 {
                                promising = true;
                                break;
                            }
                        }
                        
                        if !promising {
                            if let (true,_) = current_config.backtrack() {
                                backtracking = true;
                            }
                            else {  
                                until_switch = 0;
                                dead_contexts[k] = true;
                                nb_dead_contexts += 1;
                            }
                            continue;
                        }
                        if !backtracking {
                            current_config.include_vertex(u);
                        } else {
                            backtracking = false;
                            current_config.exclude_vertex(u);
                        }
                    }
                }
                let mut list = shared_list.lock().unwrap();
                for i in 0..local_list.len() {
                    if local_list[i] > list[i] {
                        list[i] = local_list[i];
                    }
                }
            }).unwrap();
            handles.push(handle);
        }
        for handle in handles {
            handle.join().unwrap()
        }
        return Arc::try_unwrap(final_list).unwrap().into_inner().unwrap();
    }
}


#[cfg(test)]
mod tests {
    use solver::*;
    use self::test::Bencher;

    #[bench]
    fn bench_hypercube_3(b: &mut Bencher) {
        b.iter(|| {
            let g = Graph::<u32>::hyper_cube(3);
            let result = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
            println!("{:?}", result);
        });
    }
    
    
    #[bench]
    fn bench_hypercube_4(b: &mut Bencher) {
        b.iter(|| {
            let g = Graph::<u32>::hyper_cube(4);
            let result = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
            println!("{:?}", result);
        });
    }

    #[test]
    fn test_leaf_map_with_complete_graph7() {
        let g = Graph::<u32>::complete_graph(7);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,none,none,none,none,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cycle_graph10() {
        let g = Graph::<u32>::cycle_graph(10);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,2,2,2,2,2,2,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
        assert_eq!(expected, actual);
    }

    /*
    #[test]
    fn test_leaf_map_with_wheel_graph11() {
        let g = Graph::<u32>::wheel_graph(11);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,2,2,2,none,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1);
        assert_eq!(expected, actual);
    }
    */

    #[test]
    fn test_leaf_map_with_complete_bipartite_graph_7_5() {
        let g = Graph::<u32>::complete_bipartite_graph(7,5);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,6,7,none,none,none,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cube_graph3() {
        let g = Graph::<u32>::hyper_cube(3);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,2,none,none,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cube_graph4() {
        let g = Graph::<u32>::hyper_cube(4);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,3,4,3,4,none,none,none,none,none,none,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
        assert_eq!(expected, actual);
    }
    
    #[test]
    fn test_leaf_map_with_complete_graph7_4cores() {
        let g = Graph::<u32>::complete_graph(7);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,none,none,none,none,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cycle_graph10_4cores() {
        let g = Graph::<u32>::cycle_graph(10);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,2,2,2,2,2,2,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
        assert_eq!(expected, actual);
    }

    /*
    #[test]
    fn test_leaf_map_with_wheel_graph11_4cores() {
        let g = Graph::<u32>::wheel_graph(11);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,2,2,2,none,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1);
        assert_eq!(expected, actual);
    }  
    */

    #[test]
    fn test_leaf_map_with_complete_bipartite_graph_7_5_4cores() {
        let g = Graph::<u32>::complete_bipartite_graph(7,5);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,6,7,none,none,none,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cube_graph3_4cores() {
        let g = Graph::<u32>::hyper_cube(3);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,2,none,none,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cube_graph4_4cores() {
        let g = Graph::<u32>::hyper_cube(4);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,3,4,3,4,none,none,none,none,none,none,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_complete_graph7_8cores() {
        let g = Graph::<u32>::complete_graph(7);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,none,none,none,none,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cycle_graph10_8cores() {
        let g = Graph::<u32>::cycle_graph(10);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,2,2,2,2,2,2,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
        assert_eq!(expected, actual);
    }

    /*
    #[test]
    fn test_leaf_map_with_wheel_graph11_8cores() {
        let g = Graph::<u32>::wheel_graph(11);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,2,2,2,none,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1);
        assert_eq!(expected, actual);
    }
    */

    #[test]
    fn test_leaf_map_with_complete_bipartite_graph_7_5_8cores() {
        let g = Graph::<u32>::complete_bipartite_graph(7,5);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,6,7,none,none,none,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cube_graph3_8cores() {
        let g = Graph::<u32>::hyper_cube(3);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,2,none,none,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cube_graph4_8cores() {
        let g = Graph::<u32>::hyper_cube(4);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,3,4,3,4,none,none,none,none,none,none,none];
        let actual = Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cycle_graph10_old() {
        let g = Graph::<u32>::cycle_graph(10);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,2,2,2,2,2,2,none];
        assert_eq!(expected, Solver::leaf_function(&g, 1, Strategy::Naive));
    }

    /*
    #[test]
    fn test_leaf_map_with_wheel_graph11_old() {
        let g = Graph::<u32>::wheel_graph(11);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,2,2,2,none,none];
        assert_eq!(expected, Solver::leaf_function(&g, 1));
    }
    */

    #[test]
    fn test_leaf_map_with_complete_bipartite_graph_7_5_old() {
        let g = Graph::<u32>::complete_bipartite_graph(7,5);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,6,7,none,none,none,none];
        assert_eq!(expected, Solver::leaf_function(&g, 1, Strategy::Naive));
    }

    #[test]
    fn test_leaf_map_with_cube_graph3_old() {
        let g = Graph::<u32>::hyper_cube(3);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,2,none,none,none];
        assert_eq!(expected, Solver::leaf_function(&g, 1, Strategy::Naive));
    }
    
    #[test]
    fn test_leaf_map_with_cube_graph4_old() {
        let g = Graph::<u32>::hyper_cube(4);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,3,4,3,4,none,none,none,none,none,none,none];
        assert_eq!(expected, Solver::leaf_function(&g, 1, Strategy::Naive));
    }

    #[test]
    fn test_leaf_map_of_complete_graph_upto_50() {
        let mut i = 10;
        let mut result = true;
        let none = -2147483648;
        while i <= 50 {
            let g = Graph::<u32>::complete_graph(i);
            
            //let expected: Vec<i32> = vec![0,0,2,2,3,4,3,4,3,4,none,none,none,none,none,none,none];
            let expected: Vec<i32> = (0..i+1).map(|x| {
                match x {
                    0 | 1 => 0,
                    2 => 2,
                    _ => none
                }
            }).collect();
        
            result &= expected == Solver::leaf_function_threaded(&g, 2, 1, Strategy::Naive);
            i += 1;
        }
        assert!(result);
    }

    #[test]
    fn test_leaf_map_of_cycle_graph_upto_50() {
        let mut n: i32 = 10;
        let mut result = true;
        let none = -2147483648;
        while n <= 50 {
            let g = Graph::<u32>::cycle_graph(n as u32);
            
            //let expected: Vec<i32> = vec![0,0,2,2,3,4,3,4,3,4,none,none,none,none,none,none,none];
            let expected: Vec<i32> = (0..n+1).map(|i| {
                if i == 0 || i == 1 {
                    return 0;
                }
                if 2 <= i && i < n {
                    return  2;
                }
                return none;
            }).collect();

            assert_eq!(expected, Solver::leaf_function(&g, 1, Strategy::Naive), "n: {}", n);
            n += 1;
        }
    }

    #[test]
    fn test_leaf_map_of_wheel_graph_upto_25() {
        let mut n: i32 = 4;
        let mut result = true;
        let none = -2147483648;
        while n <= 25 {
            let g = Graph::<u32>::wheel_graph(n as u32);
            
            //let expected: Vec<i32> = vec![0,0,2,2,3,4,3,4,3,4,none,none,none,none,none,none,none];
            let expected: Vec<i32> = (0..n+2).map(|i| {
                if i == 0 || i == 1 {
                    return 0;
                }
                if i == 2 {
                    return 2;
                }
                if i >= 3 && i <= (n/2 + 1) {
                    return i - 1;
                }
                if (n/2+2) <= i && i <= n - 1 {
                    return 2;
                }
                return none;
            }).collect();

            assert_eq!(expected, Solver::leaf_function(&g, 1, Strategy::Naive), "n: {}", n);
            n += 1;
        }
    }
}
