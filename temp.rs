extern crate test;
extern crate rand;

use configuration::*;
use graph::*;
use std::cmp::{max};
use std::thread;
use std::sync::{Arc, Mutex};
use std::sync::atomic::{AtomicUsize, Ordering};
use self::rand::{thread_rng, Rng};

pub struct Solver<'a> {
    g: &'a Graph<u32>
}

impl <'a> Solver<'a> {
    pub fn new(g: &'a Graph<u32>) -> Solver<'a>  {
        Solver {
            g: &g
        }
    }

    fn backtrack(&self, c: &mut Configuration<u32>) -> bool {
        while c.history_len() > 0 {
            let (_, status) = c.undo();
            if status == Status::Included {
                return true;    // There's still path to discover
            }
        }
        return false;   // No more paths to discover
    }
    
    pub fn leaf_function(&self) -> Vec<i32> {
        let mut c = Configuration::<u32>::new(&self.g);
        let nb_nodes = self.g.nb_nodes();
        let mut list = vec![i32::min_value();nb_nodes+1];
        list[0] = 0;
        let mut backtracking = false;
        let mut promising_chain = 0;
        'outer: loop {
            let u0 = c.vertex_to_add();
            if u0.is_none() {
                let i = c.subtree_size();
                let l = c.num_leaves();
                list[i] = max(list[i], l as i32);
                if self.backtrack(&mut c) {
                    backtracking = true;      
                } else {
                    break;
                }
            } else {
                let mut promising = false;
                let n = c.subtree_size();
                let m = nb_nodes + 1 - c.num_excluded();
                for i in n..m {
                    if list[i] < c.leaf_potential(i) as i32 {
                        promising = true;
                        promising_chain += 1;
                        break;
                    }
                }
                
                if !promising {
                    println!("promising chain: {}", promising_chain);
                    promising_chain = 0;
                    if self.backtrack(&mut c) {
                        backtracking = true;
                        continue;
                    }
                    else {
                        break;
                    }
                }
                if !backtracking {
                    c.include_vertex(&u0.unwrap());
                } else {
                    backtracking = false;
                    c.exclude_vertex(&u0.unwrap());
                }
            }
        }
        list
    } 

    
    fn backtrack2(c: &mut UInt32Configuration) -> bool {
        while c.history_len() > 0 {
            let (_, status) = c.undo();
            if status == Status::Included {
                return true;    // There's still path to discover
            }
        }
        return false;   // No more paths to discover
    }

    pub fn leaf_function3(g: Graph<u32>, max_threads: u16) -> Vec<i32> {
        // max_threads must be a power of two.
        let nb_nodes = g.nb_nodes();
        let mut list = vec![i32::min_value();nb_nodes+1];

        // Handles contains a `reference` to each thread that has been created.
        let mut n = max_threads / 2;    // Level of forks
        
        // Prepare the configuration for each threads.
        // we need to fork the the configuration n times.
        let mut confs = Vec::new();
        let mut configuration = UInt32Configuration::new(&g);
        let mut is_backtracking = false;
        loop {
            if confs.len() == (max_threads as usize) {
                break;
            }
            if n == 0 {
                confs.push(configuration.clone());
                Solver::backtrack2(&mut configuration);
                is_backtracking = true;
                n += 1;
                continue;
            }
            let u = configuration.vertex_to_add();
            if u.is_none() {
                let i = configuration.subtree_size();
                let l = configuration.num_leaves() as i32;
                list[i] = max(list[i], l);
                if Solver::backtrack2(&mut configuration) {
                    is_backtracking = true;      
                } else {
                    break;
                }
            }
            else {
                if is_backtracking {
                    configuration.exclude_vertex(u.unwrap());
                    is_backtracking = false;
                } else {
                    configuration.include_vertex(u.unwrap());
                }
                n -= 1;
            }
        }
        
        /*
        let list = Arc::new(Mutex::new(list));
        {
            list.lock().unwrap()[0] = 0;
        }
        let versions = Arc::new(AtomicUsize::new(0));
        */
        let mut shared_lists = Vec::new();
        let mut version_counters = Vec::new();
        for i in 0..max_threads/2 {
            shared_lists.push(Arc::new(Mutex::new(list.clone())));
            version_counters.push(Arc::new(AtomicUsize::new(0)));
        }

        let mut handles = vec![];
        for t in 0..max_threads {
            // Preclone every object that you need, so when you move
            // inside the thread, you will move only shadowed version of the 
            // original variable.
            let shared_data_index = (t/2) as usize;
            let shared_list = Arc::clone(&shared_lists[shared_data_index]);
            let builder = thread::Builder::new().name(t.to_string());
            let local_version = Arc::clone(&version_counters[shared_data_index]);
            let mut c = confs[t as usize].clone();
            
            let handle = builder.spawn(move || {
                let mut dst = vec![i32::min_value();nb_nodes+1];
                let mut first_vertex = c.history_bottom();
                let mut backtracking = false;
                let mut last_version = 0;
                // The big loop replace the recursion of the original algorithm in the research paper.
                //let mut until_sync = 10000;
                'outer: loop {
                    let u0 = c.vertex_to_add();
                    if u0.is_none() {
                        let i = c.subtree_size();
                        let l = c.num_leaves() as i32;
                        if dst[i] < l {
                            let mut shared_l = 0;
                            {
                                let mut list = shared_list.lock().unwrap();
                                shared_l = list[i];
                                list[i] = max(shared_l, l);
                            }
                            if shared_l < l {
                                let last_version1 = local_version.fetch_add(1, Ordering::Relaxed); // TODO verify ordering
                                if last_version == last_version1 {
                                    last_version += 1;
                                    dst[i] = max(shared_l, l);
                                }
                            }
                        }
                        if Solver::backtrack2(&mut c) {
                            backtracking = true;      
                        } else {
                            break;
                        }
                    } else {
                        let u = u0.unwrap();
                        if first_vertex == u {
                            break;
                        }
                        else if backtracking {
                            backtracking = false;
                            c.exclude_vertex(u);
                            continue;
                        }
                        let mut promising = false;
                        let n = c.subtree_size();
                        let m = nb_nodes + 1 - c.num_excluded();
                        let current_version = local_version.load(Ordering::Relaxed);
                        if current_version != last_version || last_version == 0 {
                            let mut lock = shared_list.try_lock();
                            if let Ok(ref mut list) = lock {
                                for i in n..m {
                                    dst[i] = list[i];
                                }
                                //dst.copy_from_slice(&list[0..]);
                                last_version = current_version;
                            }
                        }

                        for i in n..m {
                            if dst[i] < c.leaf_potential(i) as i32 {
                                promising = true;
                                break;
                            }
                        }
                        if !promising {
                            if Solver::backtrack2(&mut c) {
                                backtracking = true;
                                continue;
                            }
                            else {
                                break;
                            }
                        }  
                        c.include_vertex(u);
                        /*
                        if !backtracking {
                            c.include_vertex(u0.unwrap());
                        } else {
                            backtracking = false;
                            c.exclude_vertex(u0.unwrap());
                        }
                        */
                   }
                }
            }).unwrap();
            handles.push(handle);
        }
        for handle in handles {
            handle.join().unwrap()
        }

        for shared_list in shared_lists {
            let temp = Arc::try_unwrap(shared_list).unwrap().into_inner().unwrap();
            for i in 0..temp.len() {
                if list[i] < temp[i] {
                    list[i] = temp[i];
                }
            }
        }

        //let list = Arc::try_unwrap(list).unwrap().into_inner().unwrap();
        list
    }
}


#[cfg(test)]
mod tests {
    use solver::*;
    use self::test::Bencher;

    /*
    #[bench]
    fn bench_hypercube_3(b: &mut Bencher) {
        b.iter(|| {
            let g = Graph::<u32>::hyper_cube(3);
            let result = Solver::leaf_function3(g,2);
            println!("{:?}", result);
        });
    }
    */
    
    #[bench]
    fn bench_hypercube_4(b: &mut Bencher) {
        b.iter(|| {
            let g = Graph::<u32>::hyper_cube(4);
            let result = Solver::leaf_function3(g,2);
            println!("{:?}", result);
        });
    }


    #[test]
    fn test_leaf_map_with_complete_graph7() {
        let g = Graph::<u32>::complete_graph(7);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,none,none,none,none,none];
        let actual = Solver::leaf_function3(g,2);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cycle_graph10() {
        let g = Graph::<u32>::cycle_graph(10);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,2,2,2,2,2,2,none];
        let actual = Solver::leaf_function3(g,2);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_wheel_graph11() {
        let g = Graph::<u32>::wheel_graph(11);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,2,2,2,none,none];
        let actual = Solver::leaf_function3(g,2);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_complete_bipartite_graph_7_5() {
        let g = Graph::<u32>::complete_bipartite_graph(7,5);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,6,7,none,none,none,none];
        let actual = Solver::leaf_function3(g,2);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cube_graph3() {
        let g = Graph::<u32>::hyper_cube(3);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,2,none,none,none];
        let actual = Solver::leaf_function3(g,2);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cube_graph4() {
        let g = Graph::<u32>::hyper_cube(4);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,3,4,3,4,none,none,none,none,none,none,none];
        let actual = Solver::leaf_function3(g,2);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_complete_graph7_4cores() {
        let g = Graph::<u32>::complete_graph(7);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,none,none,none,none,none];
        let actual = Solver::leaf_function3(g,4);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cycle_graph10_4cores() {
        let g = Graph::<u32>::cycle_graph(10);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,2,2,2,2,2,2,none];
        let actual = Solver::leaf_function3(g,4);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_wheel_graph11_4cores() {
        let g = Graph::<u32>::wheel_graph(11);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,2,2,2,none,none];
        let actual = Solver::leaf_function3(g,4);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_complete_bipartite_graph_7_5_4cores() {
        let g = Graph::<u32>::complete_bipartite_graph(7,5);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,6,7,none,none,none,none];
        let actual = Solver::leaf_function3(g,4);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cube_graph3_4cores() {
        let g = Graph::<u32>::hyper_cube(3);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,2,none,none,none];
        let actual = Solver::leaf_function3(g,4);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cube_graph4_4cores() {
        let g = Graph::<u32>::hyper_cube(4);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,3,4,3,4,none,none,none,none,none,none,none];
        let actual = Solver::leaf_function3(g,4);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_complete_graph7_8cores() {
        let g = Graph::<u32>::complete_graph(7);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,none,none,none,none,none];
        let actual = Solver::leaf_function3(g,8);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cycle_graph10_8cores() {
        let g = Graph::<u32>::cycle_graph(10);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,2,2,2,2,2,2,none];
        let actual = Solver::leaf_function3(g,8);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_wheel_graph11_8cores() {
        let g = Graph::<u32>::wheel_graph(11);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,2,2,2,none,none];
        let actual = Solver::leaf_function3(g,8);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_complete_bipartite_graph_7_5_8cores() {
        let g = Graph::<u32>::complete_bipartite_graph(7,5);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,6,7,none,none,none,none];
        let actual = Solver::leaf_function3(g,8);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cube_graph3_8cores() {
        let g = Graph::<u32>::hyper_cube(3);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,2,none,none,none];
        let actual = Solver::leaf_function3(g,8);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_leaf_map_with_cube_graph4_8cores() {
        let g = Graph::<u32>::hyper_cube(4);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,3,4,3,4,none,none,none,none,none,none,none];
        let actual = Solver::leaf_function3(g,8);
        assert_eq!(expected, actual);
    }


    #[test]
    fn test_leaf_map_with_cycle_graph10_old() {
        let g = Graph::<u32>::cycle_graph(10);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,2,2,2,2,2,2,none];
        let s = Solver::new(&g);
        assert_eq!(expected, s.leaf_function());
    }

    #[test]
    fn test_leaf_map_with_wheel_graph11_old() {
        let g = Graph::<u32>::wheel_graph(11);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,2,2,2,none,none];
        let s = Solver::new(&g);
        assert_eq!(expected, s.leaf_function());
    }

    #[test]
    fn test_leaf_map_with_complete_bipartite_graph_7_5_old() {
        let g = Graph::<u32>::complete_bipartite_graph(7,5);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,6,7,none,none,none,none];
        let s = Solver::new(&g);
        assert_eq!(expected, s.leaf_function());
    }

    #[test]
    fn test_leaf_map_with_cube_graph3_old() {
        let g = Graph::<u32>::hyper_cube(3);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,2,none,none,none];
        let s = Solver::new(&g);
        assert_eq!(expected, s.leaf_function());
    }

    #[test]
    fn test_leaf_map_with_cube_graph4_old() {
        let g = Graph::<u32>::hyper_cube(4);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,3,4,3,4,none,none,none,none,none,none,none];
        let s = Solver::new(&g);
        assert_eq!(expected, s.leaf_function());
    }
}
